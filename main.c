#include <gtk/gtk.h>
#include <wiringPi.h>

struct{
    int pin;
    int state;
} typedef t_gpio;

static void gpio_write (GtkToggleButton *, gpointer);
static void gpio_read (GtkLabel *, gpointer);
void combo_changed(GtkWidget *, gpointer);

int main (int argc, char *argv[])
{
    wiringPiSetupGpio();

    t_gpio gpio17 = {17, 0};
    t_gpio gpio27 = {27, 0};

    t_gpio gpio23 = {23, 0};
    t_gpio gpio24 = {24, 0};

    GtkWidget *window;
    GtkWidget *grid;
    GtkWidget *ckbx0, *ckbx1, *ckbx2, *ckbx3;
    GtkWidget *lblGpio;

    //Uitbreiding
    GtkWidget *cbx;

    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Opdracht 5 Embedded Systems: GTK");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);
    gtk_window_set_resizable(GTK_WINDOW(window), TRUE); /* window is NOT resizable */

    grid = gtk_grid_new();
    gtk_grid_set_row_spacing (GTK_GRID (grid), 3);

    gtk_container_add (GTK_CONTAINER (window), grid);

    ckbx0 = gtk_check_button_new_with_label("BCM 17");
    g_signal_connect (ckbx0, "clicked", G_CALLBACK (gpio_write), &gpio17);
    gtk_grid_attach (GTK_GRID (grid), ckbx0, 0, 3, 2, 1);  

    ckbx1 = gtk_check_button_new_with_label("BCM 27");
    g_signal_connect (ckbx1, "clicked", G_CALLBACK (gpio_write), &gpio27);
    gtk_grid_attach (GTK_GRID (grid), ckbx1, 0, 4, 2, 1);

    /*
    ckbx2 = gtk_check_button_new_with_label("BCM 23");
    g_signal_connect (ckbx2, "clicked", G_CALLBACK (gpio_read), &gpio23);
    gtk_grid_attach (GTK_GRID (grid), ckbx2, 0, 5, 2, 1);  

    ckbx3 = gtk_check_button_new_with_label("BCM 24");
    g_signal_connect (ckbx3, "clicked", G_CALLBACK (gpio_read), &gpio24);
    gtk_grid_attach (GTK_GRID (grid), ckbx3, 0, 6, 2, 1);
    */

    //Uitbreiding

    lblGpio = gtk_label_new("Show state of GPIOS.");
    gtk_grid_attach (GTK_GRID (grid), lblGpio, 0, 8, 4, 1);

    cbx = gtk_combo_box_text_new();
    for (int i = 0; i < 28; i++){

        char sPin[10];

        sprintf(sPin, "BCM %d\0", i);
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(cbx), sPin);
    }

    gtk_combo_box_set_active(GTK_COMBO_BOX(cbx), 0);

    g_signal_connect(cbx, "changed", G_CALLBACK(combo_changed),
                     lblGpio);

    gtk_grid_attach (GTK_GRID (grid), cbx, 0, 6, 4, 1);

    gtk_widget_show_all(window);
    gtk_main();
    return 0;

}

void combo_changed(GtkWidget *widget, gpointer data)
{
    char * selected;
    int pin, state;

    selected = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(widget));

    pin = atoi(&selected[4]);
    //g_print("%d\n", pin);

    t_gpio gpio = {pin, 0};
    gpio_read(GTK_LABEL(data), (gpointer) &gpio);

}

static void gpio_write (GtkToggleButton *button, gpointer data)
{
    int pin, state;
    
    pin = ((t_gpio *)data)->pin;
    state = ((t_gpio *)data)->state;

    pinMode(pin, OUTPUT);

    if(gtk_toggle_button_get_active(button) == 1 && state == 0){
        // zet GPIO hoog
        digitalWrite(pin, HIGH);
        g_print ("GPIO BCM %d HIGH\n", pin);
       
    }
    else{
        // zet GPIO laag
        digitalWrite(pin, LOW);
        g_print("GPIO BCM %d LOW\n", pin);
    }
}

static void gpio_read (GtkLabel *label, gpointer data)
{
    int pin, state;
    char lblText [50];

    pin = ((t_gpio *)data)->pin;
    state = ((t_gpio *)data)->state;

    state = digitalRead(pin);

    sprintf(lblText, "BCM pin %d state is %s.\n\r", pin, state? "HIGH": "LOW");
    g_print(lblText);
    gtk_label_set_text(GTK_LABEL(label), lblText);

}